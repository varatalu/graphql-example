﻿
query {
  people(name: "Jeff") {
    id
    name
    gender
    hairColor
    dateOfBirth
    friends {
      name
    }
  }
}

query {
  people(hairColor: BLONDE) {
    id
    name
    gender
    hairColor
    dateOfBirth

    friends(hairColor: BRUNETTE) {
      name
      height
      weight
    }
  }
}


query {
  people{
    name
    friends {
      name
    }
  }
}



mutation {
  person {
    createPerson(input: {
      name: "NewPerson"
      gender:MALE
      hairColor:BRUNETTE
      dateOfBirth:"2010-01-02"
      height:1.2
      weight:30
    })
  }
}

mutation {
  person {
    addFriend(personId: 1, friendId: 4)
  }
}

mutation {
  person {
    createPerson(
      input: {
        name: "TestPerson"
        gender: MALE
        hairColor: BRUNETTE
        dateOfBirth: "2000-01-02"
        height: 1.2
        weight: 30
        cars: [
          {manufacturer: "Ford", modelName: "Escort", modelYear: 1987 color:SALMON }
          {manufacturer: "Toyota", modelName: "Auris", modelYear: 2005 color:SILVER }
        ]
      }
    )
  }
}

mutation {
  person {
    addCar(personId:4, input:
      {manufacturer: "Ford", modelName: "Sierra", modelYear: 1987 color:DARK_BLUE }
    )
  }
}

mutation {
  person {
    createPerson(
      input: {
        name: "Bilbo Baggins"
        gender: MALE
        hairColor: BRUNETTE
        dateOfBirth: "1995-05-02"
        height: 0.5
        weight: 100
        cars: [
          {
            manufacturer: "Ford"
            modelName: "Escort"
            modelYear: 1987
            color: SALMON
          }
          {
            manufacturer: "Toyota"
            modelName: "Auris"
            modelYear: 2005
            color: SILVER
          }
          {
            manufacturer: "BMW"
            modelName: "320d"
            modelYear: 2010
            color: BLACK
          }
        ]
        friends: [
          {
            name: "Frodo"
            hairColor: BRUNETTE
            gender: MALE
            dateOfBirth: "1989-01-05"
          }
          {
            name: "Gandalf"
            hairColor: OTHER
            gender: MALE
            dateOfBirth: "1900-01-05"
          }
        ]
      }
    )
  }
}




mutation {
  person {
    createPerson(input:{
      name:"Käsnakalle"
      height:0.5
      weight:0.1
      gender:MALE
      hairColor:OTHER
      friends:[
        {name:"Härra Krabi" gender:MALE 
          friends:
          {name:"Patrick" gender:MALE 
            friends:
            {name:"Sandy" gender:FEMALE}}}
      ]
    })
  }
}





query {
  people(name: "Käsnakalle") {
    name
    friends {
      name
      friends {
        name
        friends {
          name
        }
      }
    }
  }
}









