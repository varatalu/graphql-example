using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace ExampleGraphQLProject
{
    public class Program
    {
        public static Task Main(string[] args) => WebHost
            .CreateDefaultBuilder<Startup>(args)
            .ConfigureKestrel(options=>
            {
                options.AllowSynchronousIO = true;
            })
            .Build()
            .RunAsync();
    }
}
