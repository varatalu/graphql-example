using Data.DomainModelSpecific;
using Data.GraphQLSpecific;
using ExampleGraphQLProject.Middleware;
using GraphQL;
using GraphQL.Http;
using GraphQL.Server.Ui.Playground;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ExampleGraphQLProject
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddSingleton<IDocumentWriter, DocumentWriter>();
            services.AddSingleton<IDependencyResolver>(s => new FuncDependencyResolver(s.GetRequiredService));

            services.AddSingleton<DataContext>();
            services.AddSingleton<TestQuery>();
            services.AddSingleton<TestSchema>();
            services.AddSingleton<TestMutation>();

            //Kõik GraphQL typed peab registreerima siia
            services.AddSingleton<GenderEnumType>();
            services.AddSingleton<ColorEnumType>();
            services.AddSingleton<HairColorEnumType>();

            services.AddSingleton<PersonType>();
            services.AddSingleton<PersonMutationType>();
            services.AddSingleton<PersonInputType>();

            services.AddSingleton<CarType>();
            services.AddSingleton<CarMutationType>();
            services.AddSingleton<CarInputType>();
            services.AddSingleton<CarInputTypeWithoutIDs>();
    
            
         
            services.AddSingleton<ISchema, TestSchema>();

            services.AddLogging(builder => builder.AddConsole());
            services.AddHttpContextAccessor();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            // add http for Schema at default url /graphql
            app.UseMiddleware<GraphQLMiddleware>(new GraphQLSettings
            {
                Path = "/graphql", //overrideb , default tuleb klassist GraphQLSettings
                BuildUserContext = ctx => new GraphQLUserContext
                {
                    User = ctx.User
                },
                EnableMetrics = true
            });;

            // use graphql-playground at default url /ui/playground
            app.UseGraphQLPlayground(new GraphQLPlaygroundOptions());
        }
    }
}
