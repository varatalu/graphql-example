using System.Collections.Generic;
using System.Security.Claims;

namespace ExampleGraphQLProject.Middleware
{
    public class GraphQLUserContext : Dictionary<string, object>
    {
        // JSON WEB TOKEN AUTORISEERIMISEKS 
        public ClaimsPrincipal User { get; set; }
    }
}
