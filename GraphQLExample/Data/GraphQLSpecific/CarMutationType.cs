using Data.DomainModelSpecific;
using GraphQL.Types;

namespace Data.GraphQLSpecific
{
    /// <summary>
    /// Lihtsalt GraphQL v�li, mis sisaldab Personiga seotud operatsioone
    /// </summary>
    public class CarMutationType : ObjectGraphType
    {
        public CarMutationType(DataContext data)
        {
            Name = "Car";

            Field<StringGraphType, string>()
                .Name("createCar")
                .Argument<NonNullGraphType<CarInputType>>("input", "")
                .Resolve(context =>
                {
                    var car = context.GetArgument<CarDbRecord>("input");
                    data.AddCar(car);
                    return "added car successfully";
                });

            Field<StringGraphType, string>()
                .Name("changeOwner")
                .Argument<NonNullGraphType<IntGraphType>>("carId", "")
                .Argument<NonNullGraphType<IntGraphType>>("newOwnerId", "")
                .Resolve(context =>
                {
                    var carId = context.GetArgument<int>("carId");
                    var newOwnerId = context.GetArgument<int>("newOwnerId");
                    data.ChangeCarOwner(carId, newOwnerId);
                    return "car owner changed successfully";
                });
        }
    }
}