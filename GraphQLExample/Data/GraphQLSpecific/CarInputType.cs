using System;
using System.Collections.Generic;
using System.Drawing;
using Data.DomainModelSpecific;
using GraphQL.Types;

namespace Data.GraphQLSpecific
{
    /// <summary>
    /// Input type uue objekti lisamiseks, 
    /// </summary>
    public class CarInputType : InputObjectGraphType<CarDbRecord>
    {
        public CarInputType(DataContext data) 
        {
            Name = "CarInput";

            Field(f => f.Id,true);
            Field(f => f.OwnerId,true);
            Field(f => f.Manufacturer);
            Field(f => f.ModelName);
            Field(f => f.ModelYear);
            Field<ColorEnumType, KnownColor?>().Name("color");



        }
    }
}
