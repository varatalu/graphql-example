using System.Drawing;
using GraphQL.Types;

namespace Data.GraphQLSpecific
{
    /// <summary>
    /// GraphQL t��bid et schema peal oleks Enumid kirjas, saab nt argumentidena v�i v�ljadena kasutada
    /// </summary>
    public class GenderEnumType : EnumerationGraphType<Gender> 
    {
        public GenderEnumType()
        {
            Name = "Gender";
            Description = "description";
            
        }
    }

    public class HairColorEnumType : EnumerationGraphType<HairColor> 
    {
        public HairColorEnumType()
        {
            Name = "HairColor";
            Description = "description";
        }
    }

    public class ColorEnumType : EnumerationGraphType<KnownColor> 
    {
        public ColorEnumType()
        {
            Name = "Color";
            Description = "description";
        }
    }

    public enum Gender
    {
        MALE  = 1,
        FEMALE  = 2,
    }

    public enum HairColor
    {
        Brunette  = 1,
        Blonde  = 2,
        Ginger = 3,
        Other = 4
    }
}
