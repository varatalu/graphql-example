using GraphQL;
using GraphQL.Types;

namespace Data.GraphQLSpecific
{
    /// <summary>
    /// Terve GraphQL liidese juurp�hi
    /// </summary>
    public class TestSchema : Schema
    {
        public TestSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<TestQuery>(); //K�ik p�ringud, TestQuery.cs
            Mutation = resolver.Resolve<TestMutation>(); //K�ik muutmisoperatsioonid, TestMutation.cs
        }
    }
}
