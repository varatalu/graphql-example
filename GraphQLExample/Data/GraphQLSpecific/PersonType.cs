using System;
using System.Collections.Generic;
using System.Linq;
using Data.DomainModelSpecific;
using GraphQL.Types;

namespace Data.GraphQLSpecific
{
    /// <summary>
    /// GraphQL type c# klassist, sisaldab klassi v�ljasid, v�imaldab seoste kaudu edasi p�rida
    /// </summary>
    public class PersonType : ObjectGraphType<PersonDbRecord>
    {
        public PersonType(DataContext data)
        {


            #region Pikk kirjapilt

            Field<NonNullGraphType<StringGraphType>, string>()  //<GraphQL type , C# data type> 
                .Name("name") 
                //.Description("asdasd")
                //.DefaultValue("NO NAME")
                //.DeprecationReason("asdasd")
                .Resolve(x =>
                {
                    //siin saaks v�lja muuta v�i filtreerida enne tagastamist
                    //resolve by default otsib lihtsalt sama nimega v�lja PersonDbRecord objektilt (case insensitive),
                    //kasutada ainult siis kui nimed on erinevad v�i on vaja midagi muuta
                    return x.Source.Name; //x.Source.Height.ToString(); nt
                });

            #endregion

            //Field(f => f.Name); //default l�hike kirjapilt
            Field(f => f.Id,false);
            Field(f => f.Height,true);
            Field(f => f.Weight,true);


            Field<DateGraphType, DateTime?>().Name("dateOfBirth");
            Field<GenderEnumType, Gender>().Name("gender"); 
            Field<HairColorEnumType, HairColor>().Name("hairColor");


            Field<ListGraphType<PersonType>, IEnumerable<PersonDbRecord>>()
                .Name("friends")
                .Argument<IntGraphType>("id","")
                .Argument<HairColorEnumType>("hairColor","[Enum]")
                .Argument<GenderEnumType>("gender","[Enum]")
                .Argument<StringGraphType>("name","")
                .Resolve(ctx =>
                {
                    //ctx.Source.FriendIdsList on PersonDbRecordi v�li,
                    //seda kasutades saab seotud objekte edasi p�rida

                    if (ctx.Source.FriendIdsList == null)
                        return new List<PersonDbRecord>();

                    return data.GetPeople(ctx.Arguments)
                        .Where(friend => ctx.Source.FriendIdsList.Contains(friend.Id));
                });


            Field<ListGraphType<CarType>, IEnumerable<CarDbRecord>>()
                .Name("cars")

                .Argument<IntGraphType>("id","")
                .Argument<IntGraphType>("modelYear","")
                .Argument<ColorEnumType>("color", "[Enum]")
                .Argument<StringGraphType>("modelName", "")
                .Argument<StringGraphType>("manufacturer", "")

                .Resolve(ctx =>
                {
                    return data.GetCars(ctx.Arguments)
                        .Where(car => car.OwnerId == ctx.Source.Id);
                });


        }
       
    }
}
