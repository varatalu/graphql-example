using System;
using System.Collections.Generic;
using System.Drawing;
using Data.DomainModelSpecific;
using GraphQL.Types;

namespace Data.GraphQLSpecific
{
    /// <summary>
    /// Input type uue objekti lisamiseks, 
    /// </summary>
    public class PersonInputType : InputObjectGraphType<PersonDbRecord>
    {
        public PersonInputType(DataContext data)
        {
            Name = "PersonInput";

            //Field<NonNullGraphType<StringGraphType>, string>().Name("name").Resolve(x => { return x.Source.Name; }); Pikk kirjapilt
            Field(x => x.Name);

            Field(f => f.Height,true);
            Field(f => f.Weight,true); 
            Field(f => f.FriendIdsList,true);
            Field(f=>f.DateOfBirth,true);
            Field<GenderEnumType, Gender>().Name("gender"); //<graphql type, data type >
            Field<HairColorEnumType, HairColor>().Name("hairColor");




            Field<ListGraphType<CarInputTypeWithoutIDs>, string>().Name("cars");

            Field<ListGraphType<PersonInputType>, string>().Name("friends");
        }


    }

    public class CarInputTypeWithoutIDs : InputObjectGraphType<CarDbRecord>
    {
        public CarInputTypeWithoutIDs(DataContext data)
        {
            Field(f => f.Manufacturer);
            Field(f => f.ModelName);
            Field(f => f.ModelYear);
            Field<ColorEnumType, KnownColor?>().Name("color");
        }
    }
}
