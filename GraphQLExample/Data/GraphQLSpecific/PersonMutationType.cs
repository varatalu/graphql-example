using System.Collections.Generic;
using Data.DomainModelSpecific;
using GraphQL.Types;

namespace Data.GraphQLSpecific
{
    /// <summary>
    /// Lihtsalt GraphQL v�li, mis sisaldab Personiga seotud operatsioone
    /// </summary>
    public class PersonMutationType : ObjectGraphType
    {
        public PersonMutationType(DataContext data)
        {
            Name = "Person";

            Field<StringGraphType, string>()
                .Name("createPerson")
                .Argument<NonNullGraphType<PersonInputType>>("input", "")
                .Resolve(context =>
                {
                    var human = context.GetArgument<PersonDbRecord>("input");  
            
                    data.AddPerson(human);
                    return "added person successfully";
                });

            Field<StringGraphType, string>()
                .Name("addFriend")
                .Argument<NonNullGraphType<IntGraphType>>("personId", "")
                .Argument<NonNullGraphType<IntGraphType>>("friendId", "")
                .Resolve(context =>
                {
                    var human = context.GetArgument<int>("personId");
                    var friendId = context.GetArgument<int>("friendId");
                    data.AddFriend(human, friendId);
                    return "added friend successfully";
                });


            Field<StringGraphType, string>()
                .Name("addCar")
                .Argument<NonNullGraphType<IntGraphType>>("personId", "")
                .Argument<NonNullGraphType<CarInputTypeWithoutIDs>>("input", "")
                .Resolve(context =>
                {
                    var car = context.GetArgument<CarDbRecord>("input");
                    car.OwnerId = context.GetArgument<int>("personId");

                    data.AddCar(car);
                    return "added car successfully";
                });

          
        }
    }
}