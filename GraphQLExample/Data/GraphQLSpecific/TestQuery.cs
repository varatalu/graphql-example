using System.Collections.Generic;
using System.Linq;
using Data.DomainModelSpecific;
using GraphQL.Types;

namespace Data.GraphQLSpecific
{
    /// <summary>
    /// Queryks kutsutakse GraphQL-is Read operatsioone
    /// </summary>
    public class TestQuery : ObjectGraphType<object>
    {
        public TestQuery(DataContext data) //or Repository 
        {
            Name = "PersonQuery";

            Field<ListGraphType<PersonType>, IEnumerable<PersonDbRecord>>()// Field<GraphQL Type , Resolve Return Type>>
                .Name("people")

                //p�ringu filtrid
                .Argument<IntGraphType>("id","")
                .Argument<HairColorEnumType>("hairColor","[Enum]")
                .Argument<GenderEnumType>("gender","[Enum]")
                .Argument<StringGraphType>("name","")
               
                .ResolveAsync(async ctx =>
                {
                    //ctx sees on k�ik k�situd v�ljad, parameetrid ning muu p�ringu info
                    var loader = data.GetPeople(ctx.Arguments); //DataContext.cs
                    return loader.ToList();
                });


            Field<ListGraphType<CarType>, IEnumerable<CarDbRecord>>()
                .Name("cars")

                .Argument<IntGraphType>("id","")
                .Argument<IntGraphType>("modelYear","")
                .Argument<ColorEnumType>("color", "[Enum]")
                .Argument<StringGraphType>("modelName", "")
                .Argument<StringGraphType>("manufacturer", "")
               
                .ResolveAsync(async ctx =>
                {
                    //ctx sees on k�ik k�situd v�ljad, parameetrid ning muu p�ringu info
                    var loader = data.GetCars(ctx.Arguments); //DataContext.cs
                    return loader.ToList();
                });

        }
    }
}
