using Data.DomainModelSpecific;
using GraphQL.Types;

namespace Data.GraphQLSpecific
{
    /// <summary>
    /// Mutationiks kutsutakse GraphQL-is Create, Update, Delete operatsioone, see on nende p�hi
    /// </summary>
    public class TestMutation : ObjectGraphType
    {
        public TestMutation(DataContext data)
        {
            Name = "Mutation";

          

            Field<PersonMutationType, object>()
                .Name("person")
                .Resolve(ctx => ctx); //Resolve liigub PersonMutationType klassist l�bi, lisab k�ik v�ljad sealt

        }
    }
}
