using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Data.DomainModelSpecific;
using GraphQL.Types;

namespace Data.GraphQLSpecific
{
    /// <summary>
    /// GraphQL type c# klassist, sisaldab klassi v�ljasid, v�imaldab seoste kaudu edasi p�rida
    /// </summary>
    public class CarType : ObjectGraphType<CarDbRecord>
    {
        public CarType(DataContext data)
        {
            Field(f => f.Id);
            Field(f => f.OwnerId);
            Field(f => f.Manufacturer);
            Field(f => f.ModelName);
            Field(f => f.ModelYear);
            Field<ColorEnumType, KnownColor?>().Name("color");
         

            Field<PersonType, PersonDbRecord>()
                .Name("owner")
                .Resolve(ctx =>
                {
                    return data.GetPeople()
                        .SingleOrDefault(friend => ctx.Source.OwnerId == friend.Id);
                });


        }
       
    }
}
