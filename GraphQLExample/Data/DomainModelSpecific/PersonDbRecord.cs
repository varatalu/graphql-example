using System;
using System.Collections.Generic;
using Data.GraphQLSpecific;

namespace Data.DomainModelSpecific
{
    /// <summary>
    /// Andmebaasi kirje / EntityFrameworki klassi mudel
    /// </summary>
    public class PersonDbRecord
    {
        public PersonDbRecord()
        {
            FriendIdsList = new List<int>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public Gender Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public HairColor HairColor { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public List<int> FriendIdsList { get; set; }

        public virtual ICollection<CarDbRecord> Cars { get; set; } = new List<CarDbRecord>();
        public virtual ICollection<PersonDbRecord> Friends { get; set; } = new List<PersonDbRecord>();
    }
}
