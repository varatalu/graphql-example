using System;
using System.Collections.Generic;
using System.Drawing;
using Data.GraphQLSpecific;

namespace Data.DomainModelSpecific
{
    /// <summary>
    /// Andmebaasi kirje / EntityFrameworki klassi mudel
    /// </summary>
    public class CarDbRecord
    {
        public CarDbRecord()
        {

        }

        public int Id { get; set; }
        public int OwnerId { get; set; }
        public string Manufacturer { get; set; }
        public string ModelName { get; set; }
        public KnownColor Color { get; set; }
        public int ModelYear { get; set; }

        public PersonDbRecord Owner { get; set; }
    }
}
